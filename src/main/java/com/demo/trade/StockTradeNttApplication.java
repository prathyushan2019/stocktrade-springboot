package com.demo.trade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockTradeNttApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockTradeNttApplication.class, args);
	}

}
