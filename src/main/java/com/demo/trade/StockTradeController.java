package com.demo.trade;

import java.util.List;
import com.trade.aop.ServiceAnnotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.trade.model.StockTrade;

@CrossOrigin("*")
@RestController("StockTradeController")
public class StockTradeController {
	
	
	@Autowired
	StockService service;
	
	@RequestMapping(method=RequestMethod.GET, path="/fetch")
	public String helloWorld()
	{
		return "Welcome";
	}
	
	@RequestMapping(value="/getAllStocks", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ServiceAnnotation(targetBean = StockService.BEAN_ID, targetMethod="getAllStocks")
	public List<StockTrade> getStock() throws Exception {
		
		List<StockTrade> list = service.getAllStocks();
		return list;
	}

}
