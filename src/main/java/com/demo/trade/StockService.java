package com.demo.trade;

import java.util.List;

import org.springframework.stereotype.Component;

import com.demo.trade.model.StockTrade;

@Component(StockService.BEAN_ID)
public interface StockService {
	
	String BEAN_ID = "StockService";
	
	public List<StockTrade> getAllStocks();

}
