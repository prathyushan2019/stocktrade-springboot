package com.demo.trade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.trade.dao.StockTradeRepo;
import com.demo.trade.model.StockTrade;

@Service(StockService.BEAN_ID)
public class StockServiceImpl implements StockService{
	
	@Autowired
	StockTradeRepo repo;

	@Override
	public List<StockTrade> getAllStocks() {
		
		System.out.println("StockServiceImpl");
		
		List<StockTrade> resList = new ArrayList<StockTrade>();
		repo.findAll().forEach(person -> resList.add(person));
	    return resList;

	}
	
}
