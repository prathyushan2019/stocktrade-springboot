package com.demo.trade.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.trade.model.StockTrade;

public interface StockTradeRepo extends JpaRepository<StockTrade, String>
{
	
}
