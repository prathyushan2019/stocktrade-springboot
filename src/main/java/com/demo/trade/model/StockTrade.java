package com.demo.trade.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StockTrade {
	
	@Id
	private String symbol;
	private float bidPrice;
	private float askPrice;
	private String time;

	public String getSymbol() {
		return symbol;
	}
	
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public float getBidPrice() {
		return bidPrice;
	}
	
	public void setBidPrice(int bidPrice) {
		this.bidPrice = bidPrice;
	}
	
	public float getAskPrice() {
		return askPrice;
	}
	
	public void setAskPrice(int askPrice) {
		this.askPrice = askPrice;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "StockTrade [symbol=" + symbol + ", bidPrice=" + bidPrice + ", askPrice=" + askPrice + ", time=" + time
				+ "]";
	}
	
}
