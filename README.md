StockTrade Backend is developed using Springboot with H2(In memory) database.

The application have a rest GET call named getAllStocks in Controller: 
Which fetches the data from H2 database. This call returns the list of StockTrade objects to the client.

StockTrade Repository inherits from JPA repository.

H2 database URL : jdbc:h2:mem:test
Database contains a table called STOCK_TRADE with Column names as : SYMBOL, ASK_PRICE, BID_PRICE and TIME.

This is launched in AWS using Elastic BeanStalk under Singapore region with port 5000.
Username of H2 database to connect is : sa

Application is hosted at : http://stocktrade-env.eba-mc3yyfgn.ap-southeast-1.elasticbeanstalk.com 

The application hosted on AWS can be viewed at the following link :
http://stocktrade-env.eba-mc3yyfgn.ap-southeast-1.elasticbeanstalk.com/getAllStocks

We can also connect to the database using the following link :
http://stocktrade-env.eba-mc3yyfgn.ap-southeast-1.elasticbeanstalk.com/h2-console

Example queries to perform on database :
1. To view the table : SELECT * FROM STOCK_TRADE;
2. To Add a row to table : insert into STOCK_TRADE values ('P09:SGX',9.6, 9.3, '03/06/2020');
3. To Update a row to table : UPDATE STOCK_TRADE SET ASK_PRICE = 13 WHERE SYMBOL = 'U11:SGX';

Try to connect to the database through the console and change the values, the changes are reflected in the user interface.


Local Code Setup :
1. Clone the repository : https://prathyushan2019@bitbucket.org/prathyushan2019/stocktrade-springboot.git
2. Import the project to STS [Srping Tool Suite]/ Eclipse/ Intellij as Existing Maven Projects.
3. If importing to Eclipse/Intellij, add the Spring dependencies for Web, Rest and H2 Database.
4. Select the imported project, Run as -> Spring Boot App.

After running the app locally, use [http://localhost:5000/getAllStocks] link to see the values from Database




ReadMe for StockTrade-React [front end]

stocktrade Frontend is developed using React.

The application have a rest GET call named getAllStocks. This call returns the list of StockTrade objects.

This is launched in AWS using S3 under Singapore region with port 3000.

Application is hosted in AWS at : http://stocktrade-frontend.s3-website-ap-southeast-1.amazonaws.com/

We can connect to the database using the following link : http://stocktrade-env.eba-mc3yyfgn.ap-southeast-1.elasticbeanstalk.com/h2-console

Try to connect to the database through the console and change the values, the changes are reflected in the user interface.
Example queries to perform on database : 1. To view the table : SELECT * FROM STOCK_TRADE; 2. To Add a row to table : insert into STOCK_TRADE values ('P09:SGX',9.6, 9.3, '03/06/2020'); 3. To Update a row to table : UPDATE STOCK_TRADE SET ASK_PRICE = 13 WHERE SYMBOL = 'U11:SGX';

Local Code Setup :
1. Clone the repository : https://bitbucket.org/prathyushan2019/stocktrade-react/src/master/
2. Open the folder in VScode.
3. install npm and run "npx create-react-app stocktrade" command
4. cd stocktrade
5. run "npm install react-bootstrap bootstrap" command
6. run "npm install react-bootstrap-icons --save" command
7. run "npm start" command
8. This would host the app locally at https://localhost:3000

This would disply the table with the content from database, where the Price is displayed on calculation of (bidPrice+askPrice)/2 formula.
Trend is displayed based on the below calculation :
bidPrice < askPrice then the trend is displayed with Green arrow and vice versa.

Note : Build folder used to host the app in AWS is not uploaded as part of Source Code.